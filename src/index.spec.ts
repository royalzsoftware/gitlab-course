import {addTwoNumbers} from './index.js';

const result = addTwoNumbers(9, 10);
if (result != 19)
  throw new Error("addTwoNumbers(9, 10) expected to be equal to 19, but was " + result);
